Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{
      "ftitle": "男士休闲裤",
      "childerlist": [{
        "imageUrl": "../image/txu.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/txu.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/txu.png",
          "title": "T恤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "T恤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/txu.png",
        "title": "九分裤"
      }]
    }, {
      "ftitle": "儿童服饰",
      "childerlist": [{
        "imageUrl": "../image/tz.png",
        "title": "九分裤1"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "九分裤1"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "九分裤1"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "九分裤1"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "九分裤1"
      }]
    }, {
      "ftitle": "女士休闲裤",
      "childerlist": [{
        "imageUrl": "../image/tz.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/tz.png",
          "title": "T恤"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/tz.png",
          "title": "T恤"
        }, {
          "imageUrl": "../image/tz.png",
        "title": "长裤"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "短裤"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "七分裤"
      }, {
          "imageUrl": "../image/tz.png",
        "title": "裙裤"
      }]
    }, {
      "ftitle": "男士上衣",
      "childerlist": [{
        "imageUrl": "../image/wt.png",
        "title": "衬衣"
      }, {
          "imageUrl": "../image/wt.png",
        "title": "短袖"
      }, {
          "imageUrl": "../image/wt.png",
        "title": "背心"
      }, {
          "imageUrl": "../image/wt.png",
        "title": "外套"
      }]
    }, {
      "ftitle": "男士裤",
      "childerlist": [{
        "imageUrl": "../image/kz.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/kz.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/kz.png",
        "title": "九分裤"
      }, {
          "imageUrl": "../image/kz.png",
        "title": "九分裤"
      }]
    }, {
      "ftitle": "女士鞋子",
      "childerlist": [{
        "imageUrl": "../image/nux.png",
        "title": "高跟鞋"
      }, {
          "imageUrl": "../image/nux.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/nux.png",
          "title": "T恤"
      }, {
          "imageUrl": "../image/nux.png",
        "title": "T恤"
        }, {
          "imageUrl": "../image/nux.png",
          "title": "T恤"
      }, {
          "imageUrl": "../image/nux.png",
        "title": "T恤"
        }]
    }, {
      "ftitle": "男士鞋子",
      "childerlist": [{
        "imageUrl": "../image/nx.png",
        "title": "运动鞋"
      }, {
          "imageUrl": "../image/nx.png",
          "title": "休闲鞋"
      },  {
          "imageUrl": "../image/nx.png",
        "title": "休闲鞋"
        }, {
          "imageUrl": "../image/nx.png",
          "title": "休闲鞋"
      }, {
        "imageUrl": "../image/nx.png",
        "title": "拖鞋"
      }]
    }],
    leftid:'',
    listitem:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      leftid:0,
      listitem:this.data.list[0]
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /*
  点击跳转详情
  */
  gotodatiles: function(){
    wx.navigateTo({
      url: '../productdatile/productdatile',
    })
  },
  /*
  分类点击事件
  */ 
  checktype: function(e){
    console.log(e)
    let i = e.target.dataset.index
    console.log(e.target.dataset.index)
    this.setData({
      leftid:i,
      listitem: this.data.list[i]
    })
  }
})