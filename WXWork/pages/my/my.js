Page({

  /**
   * 页面的初始数据
   */
  data: {
    hiden:'gone',
    hidens:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'phone',
      success: function(res) {
        that.setData({
          hiden: '',
          hidens: 'gone'
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //跳转购物车
  goToCar: function () {
    wx.getStorage({
      key: 'phone',
      success: function (res) {
        console.log(res.data)
        wx.navigateTo({
          url: '../shopcar/shopcar',
        })
      },
      fail: function (res) {
        wx.navigateTo({
          url: '../login/login',
        })
      }
    })
  },
  //我的订单
  goToOrder: function () {
    wx.getStorage({
      key: 'phone',
      success: function (res) {
        console.log(res.data)
        wx.navigateTo({
          url: '../myorder/myorder',
        })
      },
      fail: function (res) {
        wx.navigateTo({
          url: '../login/login',
        })
      }
    })
  },
  //我的优惠券
  goToyhq: function () {
    wx.getStorage({
      key: 'phone',
      success: function (res) {
        console.log(res.data)
        wx.navigateTo({
          url: '../myyhq/myyhq',
        })
      },
      fail: function (res) {
        wx.navigateTo({
          url: '../login/login',
        })
      }
    })
  },
  //设置
  setting: function () {
    wx.navigateTo({
      url: '../setting/setting',
    })
  },
  //登陆
  gotologin:function(){
    wx.navigateTo({
      url: '../login/login',
    })
  }
})