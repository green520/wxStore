Page({

  /**
   * 页面的初始数据
   */
  data: {
    productName:"",
    products: {
      "originPrice": "9.9",
      "discountedPrice": "6.9",
      "tbId": "566151335478",
      "tbCombineUrl": "https://uland.taobao.com/coupon/edetail?e=YpM4Ygx9cLkGQASttHIRqVfN6/zi1vqXDZKCZIrJI1/Vl12gaWXOHUhe8q8AQ45YM+P6gXg+e02huho4lIVST5Q5wfGz/u+NdvhkyNLpsqcu2U1SwL8p1hlqjQc7+9fT",
      "originUrl": "http://h5.m.taobao.com/awp/core/detail.htm?id=566151335478",
      "couponList": [{
        "expireTip": "5天后过期",
        "condition": "满9减3",
        "amount": "3元券",
        "yyId": 2731903,
        "couponEnd": 1531152000000,
        "activityId": "566151335478-300-900-0704-0710",
        "combineUrl": "https://uland.taobao.com/coupon/edetail?e=YpM4Ygx9cLkGQASttHIRqVfN6/zi1vqXDZKCZIrJI1/Vl12gaWXOHUhe8q8AQ45YM+P6gXg+e02huho4lIVST5Q5wfGz/u+NdvhkyNLpsqcu2U1SwL8p1hlqjQc7+9fT",
        "couponRecieve": 0,
        "couponSurplus": 10000,
        "couponBegin": 1530633600000
      }, {
        "expireTip": "14天后过期",
        "condition": "满9减3",
        "amount": "3元券",
        "yyId": 2731903,
        "couponEnd": 1531929600000,
        "activityId": "9a4060bb566c41108b32e23642dd2fb0",
        "combineUrl": "https://uland.taobao.com/coupon/edetail?e=ZguTOBh2XkoGQASttHIRqVfN6/zi1vqXDZKCZIrJI1/Vl12gaWXOHRX3RPPBMotTVRd8gLSoVSPvEeIaz1E7cpQ5wfGz/u+NQg4cxp2DrgmeaD2hx1IMB2uFqp8TFaHM5SRchEGAngE87m37ab4rwg==&traceId=0bfa98e515304131229775760&id=566151335478&activityId=9a4060bb566c41108b32e23642dd2fb0",
        "couponRecieve": 690,
        "couponSurplus": 2310,
        "couponBegin": 1529596800000
      }],
      "couponExpireTip": "5天后过期",
      "couponExpireDate": 1531152000000,
      "couponValue": "3",
      "hasCoupon": 1,
      "hasValidCoupon": 1,
      "favoriatedCnt": 0,
      "commodityId": 2731903,
      "isFavor": 1,
      "isFreePostage": 1,
      "source": 1,
      "title": "红米NOTE5手机壳小米5X手机壳NOTE5a保护套5S磨砂硅胶防摔5C全包",
      "text": "",
      "saleCnt": 1053,
      "tbToken": "￥GiKc0scGR19￥",
      "share": {
        "imageUrl": "https://img.alicdn.com/imgextra/i4/1728326437/TB2tQtze3KTBuNkSne1XXaJoXXa_!!1728326437.jpg",
        "title": "红米NOTE5手机壳小米5X手机壳NOTE5a保护套5S磨砂硅胶防摔5C全包",
        "text": "这个商品有张专属的隐藏优惠券，快点来抢",
        "url": "http://m.shellselect.vip/#/productDetail/2731903?promotionCode=856728"
      },
      "coverList": [{
        "type": 1,
        "content": "https://img.alicdn.com/imgextra/i4/1728326437/TB2tQtze3KTBuNkSne1XXaJoXXa_!!1728326437.jpg_500x500q90.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 1,
        "content": "https://img.alicdn.com/i2/2382617777/TB2IMlqhVooBKNjSZPhXXc2CXXa_!!2382617777.jpg_500x500q90.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 1,
        "content": "https://img.alicdn.com/i3/2382617777/TB2uUOFwMaTBuNjSszfXXXgfpXa_!!2382617777.jpg_500x500q90.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 1,
        "content": "https://img.alicdn.com/i2/2382617777/TB2qas3dHuWBuNjSszgXXb8jVXa_!!2382617777.jpg_500x500q90.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 1,
        "content": "https://img.alicdn.com/i1/2382617777/TB26VoqaP7nBKNjSZLeXXbxCFXa_!!2382617777.jpg_500x500q90.jpg",
        "height": 0,
        "width": 0
      }],
      "descList": [{
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i2/2382617777/TB2X2A0dXooBKNjSZFPXXXa2XXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i3/2382617777/TB2J57sdTXYBeNkHFrdXXciuVXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i3/2382617777/TB2KsQ.lVuWBuNjSspnXXX1NVXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i1/2382617777/TB2ztwuXoD.BuNjt_ioXXcKEFXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i3/2382617777/TB2SAwClYSYBuNjSspiXXXNzpXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i1/2382617777/TB2.Ztymf1TBuNjy0FjXXajyXXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i2/2382617777/TB2rIQsXlgXBuNjt_hNXXaEiFXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }, {
        "type": 2,
        "content": "https://img.alicdn.com/imgextra/i2/2382617777/TB2HVdymb1YBuNjSszhXXcUsFXa_!!2382617777.jpg_2200x2200Q50s50.jpg",
        "height": 0,
        "width": 0
      }],
      "type": 1,
      "commission": "0.77",
      "pid": "mm_41331782_43656630_631242881"
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   var str = options.product;
    //let product = JSON.parse(options.product) 
    this.setData({
      productName: str
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})