Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/313e67c1063613799dedccd7a295bd77.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "夏日潮T",
      "text": "",
      "url": "shell://c?id=115",
      "weight": 0
    }, {
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/f885e1130f02487dc1fbc563f6ca23bf.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "男女鞋",
      "text": "",
      "url": "shell://c?id=112",
      "weight": 0
    }, {
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/ef2978814fd013cb8e12fba37c6ddfeb.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "防晒专题",
      "text": "",
      "url": "shell://c?id=110",
      "weight": 0
    }, {
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/313e67c1063613799dedccd7a295bd77.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "夏日潮T",
      "text": "",
      "url": "shell://c?id=115",
      "weight": 0
    }, {
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/f885e1130f02487dc1fbc563f6ca23bf.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "男女鞋",
      "text": "",
      "url": "shell://c?id=112",
      "weight": 0
    }, {
      "imageUrl": "http://shell-select-public-bucket.oss-cn-hangzhou.aliyuncs.com/ef2978814fd013cb8e12fba37c6ddfeb.jpg",
      "linkType": 2,
      "isVisible": 1,
      "title": "防晒专题",
      "text": "",
      "url": "shell://c?id=110",
      "weight": 0
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    setTimeout(function () {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      // 停止下拉动作
      wx.stopPullDownRefresh();
      wx.showToast({
        title: '刷新成功',
      })
    }, 2000)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  //跳转到相应类列表
  goTolistfind:function(e){
    wx.navigateTo({
      url: '../findlist/findlist?title=' + e.currentTarget.dataset.index.title + '&img=' + e.currentTarget.dataset.index.imageUrl,
    })
  }
})