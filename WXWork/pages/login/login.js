var interval = null //倒计时函数
var phone = null;
var code = null;
var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;//手机号正则
Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: '请选择日期',
    fun_id: 2,
    time: '获取验证码', //倒计时 
    currentTime: 61
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  getCode: function (options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 61,
          disabled: false
        })
      }
    }, 1000)
  },
  getVerificationCode() {
   
    if (phone == null || phone == '') {
      wx.showToast({
        title: '手机号不能为空',
      })
    } else if (phone.length != 11){
      wx.showToast({
        title: '手机号长度不正确',
      })
    } else if (!myreg.test(phone)){
      wx.showToast({
        title: '手机号不正确',
      })
    }else{
      this.getCode();
      var that = this
      that.setData({
        disabled: true
      })
    }
  },
  //手机输入
  inputKeyone:function(e){
    phone = e.detail.value;
  },
  //验证吗输入
  inputKeytwo:function(e){
    code = e.detail.value;
  },
  //登陆
  goToSucces:function(){
    if(phone==null||phone==''){
        wx.showToast({
          title: '手机号不能为空',
        })
    } else if (!myreg.test(phone)) {
      wx.showToast({
        title: '手机号不正确',
      })
    } else if (phone.length!=11) {
      wx.showToast({
        title: '手机号长度不对',
      })
    }else if(code==null||code==''){
      wx.showToast({
        title: '验证码不能为空',
      })
    } else if (code.length!=4) {
      wx.showToast({
        title: '验证码长度不对',
      })
    }else{
      wx.setStorage({
        key: 'phone',
        data: phone,
      });
      wx.navigateBack({
        delta: -1
      });
      var pages = getCurrentPages();
      var currPage = pages[pages.length - 1];   //当前页面
      var prevPage = pages[pages.length - 2];  //上一个页面

      //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
      prevPage.setData({
          hiden: '',
          hidens: 'gone' 
      })
    }
  }
})